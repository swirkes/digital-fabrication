+++
title = '08 Electronics Design'
author = 'Shane Wirkes'
date = 2024-03-19T08:42:51+02:00
draft = false
+++

Designing and milling a custom circuit board...
<!--more-->

## Group Work

Tomi was our group leader, documenting the use of measurement devices. Here's a [link](https://tomimonahan.gitlab.io/digital-fabrication/electronics-design-group-assignment.html)

## Idea

I wanted to design a board to connect a DAC (digital to analog converter) and an amplifier to the RP2040 in order to play sound from it.

At first I was interested in making a reallly simple 16 bit DAC with an R 2R resistor network.

![alt text](../../images/assignment8/converted/IMG_5931.jpg)

However, after browsing the fablab inventory and seeing the much more powerful MCP4725 chip, I decided to go with that and work on building a circuit for it.

I referenced the data sheet for the DAC to see how to interface with the RP2040 and found I could do so with I2C. Additionally I found that I needed an amplifier to boost the audio signal so I checked the fab lab inventory and found the LM4871. 

The LM4871 contained an example circuit which I relied on heavily in designing the circuit board. 

![example circuit](<../../images/assignment8/converted/Screenshot 2024-03-21 at 12.35.32.jpg>)

With the DAC and Amplifier pieces connected to the RP2040, I added some breakout pins for the rest of the board per the assignment. 

![schematic](<../../images/assignment8/converted/Screenshot 2024-03-21 at 12.14.59.jpg>)

After finishing the schematic, I went into the PCB designer and started untangling the rat's nest. It took me quite a while to get the all the traces properly placed and it would have been way easier if I'd used a double-sided board, but I took it as a challenge and although it may not win any awards, it's passed all the design rule checks.

I added a border to the board and then added the copper as a Filled Zone. 

![pcb with copper fill](<../../images/assignment8/converted/Screenshot 2024-03-21 at 12.15.19.jpg>)

I made sure that the track size was set at 0.4 mm so that it would be a size compatible with the milling machine. 

![track size](<../../images/assignment8/converted/Screenshot 2024-03-20 at 11.41.42.jpg>)

The next thing I did was Plot the board and the drill files so that it was saved as a gerber file. 

![plot](<../../images/assignment8/converted/Screenshot 2024-03-21 at 12.26.53.jpg>)

![the plot thickens](<../../images/assignment8/converted/Screenshot 2024-03-21 at 12.27.02.jpg>)

Finally, I exported a Bill of Materials from KiCad as a csv file and edited a bit to make it easy to reference when it's time to assemble.

![BOM](<../../images/assignment8/converted/Screenshot 2024-03-21 at 12.17.34.jpg>)

With the gerber files ready, I was ready to mill. I used CopperCAM to set up the file as we did a few weeks ago.

After milling the engraving layer, I noticed that some of the traces pulled up or were destroyed during the milling. I double-checked the settings in KiCad and they were fine. After asking for help, it was determined that the problem was likely due to the material not being all the way adhered to base of the milling machine. 

![poorly milled board](../../images/assignment8/converted/IMG_5947.jpg)

I milled the board again, this time making sure the pcb was perfectly flat and well adhered with tape. This time, it looked much better, but there was still a section towards the bottom where it didn't cut deep enough.

![missing traces](../../images/assignment8/converted/IMG_6006.jpg)

I adjusted the Z-position to 0.05 mm lower than where it was and ran the process again. That did the trick and the board was properly milled. 

![traces fixed](../../images/assignment8/converted/IMG_6007.jpg)

## Populating Components

I populated the board with all the necessary components, but I found that the footprint for the switch was slightly off. This created a situation where the foot that was supposed to be connected to nothing was actually touching ground, rendering the switch inoperable. 

![semi-populated board](../../images/assignment8/converted/IMG_6013.jpg)

Kris updated the footprint in the library so that I could try again.

![updated switch footprint](<../../images/assignment8/converted/Screenshot 2024-04-04 at 11.45.00.jpg>)

I attempted to test the output of the board with a script I wrote with the help of ChatGPT. Essentially, it uses bit-shifting to create a triangle wave which then varies in amplitude and frequency.

```#include <Wire.h>

#define MCP4725_ADDR 0x60

// Variables for triangle wave parameters
float amplitude = 2047.0; // Initial amplitude (half of DAC range)
float frequency = 0.1;    // Initial frequency (adjust as needed)
float phase = 0.0;        // Phase offset

// Function to set DAC value
void setDAC(uint16_t dacValue) {
  // Convert dacValue to two bytes
  byte data[2];
  data[0] = (dacValue >> 8) & 0xFF;
  data[1] = dacValue & 0xFF;
  // Write data to MCP4725
  Wire.beginTransmission(MCP4725_ADDR);
  Wire.write(0x40); // Write DAC value command
  Wire.write(data, 2); // Write DAC value data
  Wire.endTransmission();
}

void setup() {
  // Initialize I2C communication
  Wire.begin();
}

void loop() {
  static uint16_t value = 0;

  // Calculate triangle wave value based on amplitude, frequency, and phase
  float triangleValue = amplitude * (2.0 * abs(2.0 * (phase - 0.25) - 1.0) - 1.0);
  
  // Convert triangle wave value to DAC range
  value = (uint16_t)map(triangleValue, -amplitude, amplitude, 0, 4095);

  // Set DAC value
  setDAC(value);

  // Increment phase based on frequency
  phase += frequency;
  if (phase >= 1.0) {
    phase -= 1.0; // Wrap phase back to 0 after one cycle
  }

  // Adjust amplitude and frequency for variation
  amplitude += 5.0;   // Increment amplitude (adjust as needed)
  frequency += 0.001; // Increment frequency (adjust as needed)

  // Delay for a short period to control loop speed
  delay(10); // Adjust as needed for desired loop speed
}
```

Unfortunately, I didn't get any sound and when I tried to analyze the DAC output, I accidentally shorted it and blew it up. 

To move forward, I need to mill another board, so this time I will only populate the components necessary for the DAC at first and then test that before moving onto the amp and then finishing the board.

## Troubleshooting

I worked with Kris to troubleshoot the board. I learned a lot about the troubleshooting process. [Here](https://gitlab.com/kriwkrow/input-devices-xiao-demo) is the repository Kris made.

## Simplify Design

I couldn't get anything out of the DAC. Kris built a board that only contained the DAC chip with the RP2040 and an output for the signal. We put this together and still couldn't figure out the problem. After some Googling we found a script to search for a connected I2C device by address. This quicklly revealed the problem.

```// SPDX-FileCopyrightText: 2023 Carter Nelson for Adafruit Industries
//
// SPDX-License-Identifier: MIT
// --------------------------------------
// i2c_scanner
//
// Modified from https://playground.arduino.cc/Main/I2cScanner/
// --------------------------------------

#include <Wire.h>

// Set I2C bus to use: Wire, Wire1, etc.
#define WIRE Wire

void setup() {
  WIRE.begin();

  Serial.begin(9600);
  while (!Serial)
     delay(10);
  Serial.println("\nI2C Scanner");
}


void loop() {
  byte error, address;
  int nDevices;

  Serial.println("Scanning...");

  nDevices = 0;
  for(address = 1; address < 127; address++ )
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    WIRE.beginTransmission(address);
    error = WIRE.endTransmission();

    if (error == 0)
    {
      Serial.print("I2C device found at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.print(address,HEX);
      Serial.println("  !");

      nDevices++;
    }
    else if (error==4)
    {
      Serial.print("Unknown error at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.println(address,HEX);
    }
  }
  if (nDevices == 0)
    Serial.println("No I2C devices found\n");
  else
    Serial.println("done\n");

  delay(5000);           // wait 5 seconds for next scan
}
```

According to the ADAFruit Library which was being used, the address of the chip could be any integer between 60 - 65. However, running the script, we quickly found out that the address was 66. As soon as we changed this setting, everything from the DAC worked as expected. 

I tried the same code on my board and was able to get the triangle wave out of the DAC.

![triangle wave from DAC](<../../images/assignment8/converted/Screenshot from 2024-04-16 10-12-58.jpg>)

However, there still wasn't any sound. Kris iterated on the original DAC board by adding the amplifier chip. In this case, he was able to produce sound. The first iteration, he removed the potentiometer for volume control so that there were just resistors. After proving this to work, he added a potentiometer to adjust the volume. The only difference here between my board and his was the placement of the potentiometer. In my board, I placed the potentiometer where R4 is in the picture below.

![placement of the potentiometer](<../../images/assignment8/Screenshot 2024-04-18 at 12.16.29.png>)

I reworked my board to follow this placement, changing out the switch for something easier to solder as well.

That's the furthest I've gotten...To Be Continued.

