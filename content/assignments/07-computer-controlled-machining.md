+++
title = '07 Computer-Controlled Cutting'
author = 'Shane Wirkes'
date = 2024-03-15T08:42:51+02:00
draft = false
+++

Milling a simple table designed in CAD...
<!--more-->

## Designing an item in Fusion360

I designed a simple table using parametric settings related to the material I would be using. I followed a tutorial by [What? Make Art](https://www.youtube.com/watch?v=I6FEMdtcrpI&list=PL8G4GiXpgTvLtq4P3ClYG9IC1Cyg-b84X&index=64) and modified it to my own taste and values.

![parametric settings](<../../images/assignment7/converted/Screenshot 2024-03-21 at 10.46.17.jpg>)

After creating the design with each piece as a separate component, I made another component to represent the wood I would be using to cut the table from. Using the Joint command, I was able to place the pieces onto the board.

![using joints to lay out pieces](<../../images/assignment7/converted/Screenshot 2024-03-21 at 10.45.38.jpg>)

After placing the pieces, I was able to move them around to lay them out nicely.

![laying out pieces onto board](<../../images/assignment7/converted/Screenshot 2024-03-21 at 10.45.30.jpg>)

With the pieces laid out, I exported the file as .xdf and was ready to move over to the milling machine. To do this, I created a sketch on the board surface and then projected the components onto this new sketch. Then, I right-clicked the sketch and chose export to export it as a .dxf. This seems to be the best way to do it so that it saves exactly as is.

![pieces laid out](<../../images/assignment7/converted/Screenshot 2024-03-21 at 10.44.37.jpg>)


## Setting up the Recontech 1312 CNC Machine 

Using VCarve, I set the heigh, width and thickness of the material with a little extra on the thickness since I'm using a sacrificial layer. The XY-Datum position is set to the south west corner.

I used the pocket toolpath for the joints, since I designed the table to have joint inputs of half the thickness of the material.

![pocket toolpath](<../../images/assignment7/converted/Cnc 2.jpg>)

We used a 6mm endmill bit for this job. 

![endmill bit 6mm](<../../images/assignment7/converted/Cnc 4.jpg>)

The bit has 2 flutes which facilitate chip extraction and informs the rates of the feeds and speeds. 

![bit with two flutes](<../../images/assignment7/converted/Cnc 3.jpg>)

There is a reference guide for calculating the feed rate. Following the table for the type of material you're using, you multiply number of flutes by chip load by spindle speed to calculate the feed rate. 

![calculate feed rate](<../../images/assignment7/converted/Cnc 5.jpg>)

The plunge rate can be calculated roughly with feed rate divided by 3.

![feeds and speeds](<../../images/assignment7/converted/Cnc 6.jpg>)

In the toolpaths menu, you can see what path the bit will follow and edit it if necessary.

![visualizing tool path](<../../images/assignment7/converted/Cnc 9.jpg>)

Next we add tabs to the design. Tabs leave a little bit of material between the piece and the rest of the wood. This acts to hold the piece in place while it is being cut. The tabs are removed in post-processing. 

![add tabs](<../../images/assignment7/converted/Cnc 10.jpg>)

Place the tabs liberally so that the pieces stay secure during cutting.

![place tabs](<../../images/assignment7/converted/Cnc 11.jpg>)

When you've placed the tabs and set everything else up properly, you can check a render of the layout and see exactly what you'll end up with after the job is done.

![See the layout rendered](<../../images/assignment7/converted/Cnc 13.jpg>)

The next step is to save the tool paths.

![save tool paths](<../../images/assignment7/converted/Cnc 14.jpg>)

The tool paths are saved as GCode, basically giving x, y, and z coordinates.

![gcode](<../../images/assignment7/converted/Cnc 15.jpg>)

Moving over to the CNC Machine, the bed has a vaccuum attached to it which sucks the material tightly to the base so that it doesn't move during manufacturing.

![vaccuum bed](<../../images/assignment7/converted/Cnc 16.jpg>)

There is a sheet on the wall detailing the steps for setting up the machine. 

![cnc machine set up sheet](<../../images/assignment7/converted/Cnc 18.jpg>)

We placed a spoilboard on the table so that the drill doesn't carve into the table.

![place spoilboard and material](<../../images/assignment7/converted/Cnc 19.jpg>)

It's important to wear safety glasses and gloves when in the CNC room!

![wear safety glasses and gloves](<../../images/assignment7/converted/Cnc 20.jpg>)

There is a pressure guage and two valves for turning on the vaccuum on either side of the table. Make sure the pressure valve stays in the green.

![pressure guage and vacuum throughput](<../../images/assignment7/converted/Cnc 21.jpg>)

Turn on the vaccuum pump with this button (make sure your headphones are on)

![vaccuum pump button (wear headphones)](<../../images/assignment7/converted/Cnc 22.jpg>)

Check that the pressure guage stays in the green...

![pressure guage](<../../images/assignment7/converted/Cnc 23.jpg>)

Set the Z-Position in the Mach3 software. There is an electronic connection detected when the piece touches the drill bit. Set the X and Y position as well with reference to the material in the south west corner.

![setting the z-position](<../../images/assignment7/converted/Cnc 24.jpg>)

Once the GCode is imported into Mach 3, and the position is calibrated, you can turn on the machine from outside the room. Press the green button to run it and press the red button if something goes wrong and you need to shut it down immediately.

![turn on the machine from outside](<../../images/assignment7/converted/Cnc 25.jpg>)

We ran the pocket cutting job first and then went back to do the cut outs. With the pieces cut, this is what it looked like.

![pieces cut!](<../../images/assignment7/converted/Cnc 26.jpg>)



## Post Processing

After cleaning up the space, we took the material to the wood shop for post processing. The first thing to do is remove the tabs with a chisel to free the pieces from the board.

![remove tabs](<../../images/assignment7/converted/Cnc 27.jpg>)

After that, connect the router to a vaccuum cleaner and run it along the edges to create smooth edges where the tabs were.

![router to remove tabs](<../../images/assignment7/converted/Cnc 28.jpg>)

You can plug the router into the vaccuum so that it turns on when the router turns on.

![hook up for vaccuum](<../../images/assignment7/converted/Cnc 30.jpg>)

I sanded all the pieces by hand to get an even finish and tested that it fits together. The joints fit very nicely, however, it definitely needs glue to be a stable piece. I think I'll paint it before gluing.

![completed stool](../../images/assignment7/converted/camphoto_162216788.jpg)