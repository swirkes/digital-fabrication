+++
title = '01 Project Management'
author = 'Shane Wirkes'
date = 2024-01-31T11:03:51+02:00
draft = false
+++

This first assignment documents the making of my website as well as my process for optimizing images.
<!--more-->

## Website Documentation

Initially, I created my website with a mix of HTML, CSS, and a bit of JavaScript to construct and style the web pages. However, when I began creating the third page of my site (this assignment), I grew weary of how meticulous it was to write new content in HTML and keep all the style elements consistent across each page. I decided instead to use a static site generator, in this case [Hugo](https://gohugo.io/) and I chose a terminal-style theme, [risotto](https://github.com/joeroe/risotto) for a minimal *hackerman* vibe.

I decided to use Visual Studio Code (VS Code) to develop my site because of my familiarity with it and its integrated terminal. Although it may be overkill for this application, I use it for a lot of other software development projects, so it's already set up on my machine.

![Visual Studio Code light theme screenshot](../../images/VS_Code_light.jpg)
*Visual Studio Code (light theme)*

### Version Control with Git and GitLab

I manage my website's codebase using Git for version control, which allows me to track changes and revert to earlier versions if necessary. The local repository on my machine is synchronized with a remote repository hosted on GitLab. This setup not only serves as a backup but also simplifies collaboration with others.

#### Common Git Commands

Here are the main commands I've been using:

- **git add:** This command is used to add files to the staging area. For instance, 'git add <filename>' will stage a specific file, while 'git add .' will stage all changes.
- **git commit:** This command is used to save the staged changes along with a descriptive message, for example, 'git commit -m "Descriptive message here".'
- **git push:** This command is used to upload the local repository content to a remote repository.
- **git pull:** This command is used to fetch and download content from a remote repository and immediately update the local repository to match that content.
- **git branch:** This command creates a copy of the existing directory and stores it separately so that you can make changes and test them before changing the live setup.

After having used Git for a while, I've found that using ```bash git add .``` to add all changes to the staging area is more efficient than listing files individually, especially when working with multiple files that need to be committed.

### Static Site Generation and CI/CD

The website is a static site, which means it consists of HTML files that are served exactly as stored. A static site generator (Hugo) was used in this case to expedite the development process by generating HTML files from templates and markdown files. This allows me to write my posts in markdown format, which is more convenient. The deployment of the website is handled through GitLab's Continuous Integration/Continuous Deployment (CI/CD) pipeline. This automated process allows for changes pushed to the repository to be automatically built and deployed to the hosting environment, ensuring the live site is always up-to-date.

### Converting an HTML site to static site generator

I want to detail my process for moving from a pure HTML page to a static site generator. There were several times I needed to troubleshoot to get it online so it may be useful for future reference.

### Backup the old site (create a branch)

Before making any significant changes, I ensured I could go back to the original state of the website if necessary. This was done by creating a new branch in my Git repository, effectively snapshotting the site before its transformation.

```bash
git checkout -b old-site-backup
```

### Install Hugo and add 'risotto' theme as submodule
I deleted everything in the folder except the .git sub-folder and initiated a new Hugo project within the same directory. After that, I added the 'risotto' theme as a submodule to keep it up-to-date and separate from my own content.

```bash
hugo new site .
git submodule add https://github.com/joeroe/risotto.git themes/risotto
```

### Convert HTML pages to MD
Next, I converted the pages from HTML to MD. The optimal way to do this was by asking ChatGPT.

![markdown vs HTML](../../images/md-vs-html.jpg)
*Almost 100 more lines of HTML to get the same kind of result in markdown!*

### Reconfigure .gitlab-ci.yml
Initially, I tried to preserve the .gitlab-ci.yml file and edit it, however, I found it easier to simply delete the file from the repository online and then add a new one with GitLab's wizard. This ensured that everything was set up properly.

![creating a new .gitlab-ci.yml](../../images/yaml-creation.jpg)

### Add .gitignore with /public/
According to GitLab's [documentation](https://docs.gitlab.com/ee/tutorials/hugo/#create-a-gitlab-project), it's important *not* to upload the public folder because it can cause conflicts later on. So I added a .gitignore file to my folder and added '/public/' to it.

```bash
echo "/public/" >> .gitignore
```

### Test locally
Using the terminal command ```hugo server --buildDrafts```, I was able to preview the website locally by visiting ```http://localhost:1313/digital-fabrication/```. 

This was great because it will automatically load changes whenever I save a file, so I can make many little changes and tweaks and get instant feedback.

### Push updated repository to Main
After I was happy with the way the site looked locally, I was ready to push it to the **Main** branch of my GitLab repository. I checked what branch I was on with ```git branch```, which showed me the two branches on my repo with a * next to the one I was currently in:

```bash
backup-branch
* main
```

Following this check, I went ahead with the following familiar routine:

```bash
git add .
git commit -m "Add new Hugo site to repo"
git push origin main
```

So far so good...

### Check Pipeline
I visited the Pipeline page and saw that the the updates had been successfully deployed, meaning that my website should be live. However, when I clicked the link, I got a **404 error** letting me know that my site could not be found.

![Pipelines](../../images/pipelines.jpg)
*navigating to pipelines & the green check marks showing successful deployment*

### Troubleshoot
After checking the *yaml* file, the *config.toml*, and scouring the web for an answer, I checked the terminal output from the jobs going through the pipeline. This revealed to me that the number of Pages on my site was 0. Upon further inspection, I realized that I had all of my pages marked as drafts in the so-called *frontmatter* or metadata.

```bash
+++
title = 'About'
date = 2024-01-31T10:38:35+02:00
draft = true
+++
```
*sample frontmatter from one of my pages*

### Success!
Changing the line in the frontmatter to ```draft = false``` on all my pages solved the problem and the website became visible in the same place the old one had been.

I was relieved to get the new site online, but the fact that I had the old one on another branch was key to preserving my sanity!


## Capturing Images

To document my work, I frequently take screenshots using the built-in shortcut on my MacBook Air ```CMD + Shift + 4```. This command brings up a crosshair cursor allowing me to select the exact area of the screen I want to capture. The screenshot is then saved automatically to a folder I have previously chosen. I can also use ```CMD + Shift + 3``` to immediately take a picture of the entire screen. The screenshots are automatically saved as PNG files so I'll need to convert them.

### Image Optimization

After capturing screenshots, I optimize them using the command line tool [ImageMagick](https://imagemagick.org/), a free, open-source image editor. This program is a beast and can do amazing things. At this point I only need to do three things:

- fit into a 1920x1920px rectangle 
- save it in JPG format 
- 60-80 percent quality

I achieved this with the following terminal command: 

```bash
magick input.png -resize 1920x1920\> -quality 80 output.jpg 
```
Breaking this down:
- ```magick``` invokes the ImageMagick program
- ```input.png``` is the image I want to edit
- ```-resize 1920x1920\>``` only resizes the image if it is larger than 1920x1920 px and mantains its proportions
- ```-quality 80``` sets the quality of the image (how much compression)

This screenshot gives a sense of just how much an image can be optimized without losing significant quality on the web (Screenshot...is the input image):

![image compression](../../images/image-sizes.jpg)
