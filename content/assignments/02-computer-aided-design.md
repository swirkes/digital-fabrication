+++
title = '02 Computer-Aided Design'
author = 'Shane Wirkes'
date = 2024-02-07T11:33:51+02:00
draft = false
+++

Documenting some CAD tool workflows as I learn to use them.
<!--more-->

# Vector Drawing Software

I decided to use [Inkscape](https://inkscape.org/) because it's been sitting on my computer unused for a few years now and this seemed like a great opportunity to take a test drive. I like to use open-source software whenever possible and since I'm not planning to do anything too crazy, I thought it could be a good tool.

## Create a new project

When Inkscape opens, you can start a new file with a template based on document or screen size. You can open an existing project or create a custom one. For my design, I chose an A4 sized document. 

## Shapes

Shapes were the main source of content for my project. The left sidebar shows several common shapes including cubes and polygons. I started with rectangles and circles to lay out the general design of my synth's faceplate. 

![shapes sidebar](../../images/assignment2/image1.jpg)
*shapes on the sidebar*

I wanted to create a hex nut shape to make the audio input jacks a little more realistic. To do this, I chose the polygon option which offers several options after clicking. Choosing the pentagon (as opposed to the star), I added 6 corners without any rounding. A simple hexagon was created.

![Hex shape](../../images/assignment2/image6.jpg)
*creating a hexagon from the shape menu*

## Fill

You can color your shapes with the Fill and Stroke dialog. This can be found in the Object menu or by pressing ```CMD + Shift + F```. There are many options to choose from including patterns and gradients. 

![fill and stroke panel](../../images/assignment2/image2.jpg)

## Order

You can shift the order of a selected element from the toolbar at the top of the screen. There are options for moving up or down by one layer as well as sending the layer to the bottom or the top.

![order](../../images/assignment2/image7.jpg)

*change the order of the selected item*
## Group

Another useful tool in Inkscape is the Group command executed with ```CMD + G``` and deactived with ```CMD + Shift + G```. This command combines the selected elements so that they can be moved and resized as a single unit. I used it to keep things symmetrical and aligned while playing with the layout.

![group](../../images/assignment2/image5.jpg)
*group selected elements for better organization*

## Text

Clicking the large A on the sidebar or pressing ```CMD + T``` lets you add text boxes to your design. As you can see in the photo, the toolbar at the top transforms into a font type and size dialog. You can also access this information on the right-hand size of the program.

![text editor](../../images/assignment2/image9.jpg)
*side and top toolbars show the text editor*

## Import

It's probably not a great idea to introduce an image into a vector design, but I wanted to show the waterfall graph from the radio on this mockup so that it wasn't just an empty rectangle at the top. I did this by selecting Import from the File menu (also accessible with ```CMD + I```). After selecting the photo, I was presented with several options for rendering the image. I stuck with the presets as seen below. After pressing OK, the image was loaded into my design and was easily resized to fit.

![import menu](../../images/assignment2/image8.jpg)
*several options for importing images*

## Alignment

I used the Align tool quite a lot to get everything layed out harmoniously. You can find this at the bottom of the Object menu or by pressing ```CMD + Shift + A```. In my case, it was already available on the right hand side of the window as seen in the photo below. Many options for aligning and distributing elements are available. You basically select the elements you want to align, choose an option from the "Relative to: " dialog box and then click the alignment you want. 

![Align panel](../../images/assignment2/image3.jpg)
*the right side of the program window had the Align Dialog open already*

## Export Final Image 

When I got my project where I wanted, I exported it from the File menu or ```CMD + Shift + E```. This opened the dialog seen below where I chose to export a jpg.
![export menu](../../images/assignment2/image10.jpg)
*export menu*

Here is the final image exported as a jpg: 

![final image radio synth](../../images/assignment2/radio-synth-vector.jpg)
*final image: radio synth*

# Raster Graphics Software

I've heard a lot about [Gimp](https://gimp.org) over the years and for the same reasons I chose Inkscape, I decided to give it a try. For this documentation, I simple imported a png, cropped it, and exported it as a lower resolution, smaller sized jpg.

## Import

You can import an existing image directly from the file menu. I chose "Open as Layers" to also bring in the image as a layer:

![import image](../../images/assignment2/image24.jpg)

*importing an image*

## Crop
There is a **crop** button in the tool panel on the left side of the screen:

![crop button](../../images/assignment2/image25.jpg)
*crop button*

Choosing this turns your cursor into a rectangular selection box. I selected only the graph window from my image and pressed enter:

![crop window](../../images/assignment2/image26.jpg)
*select the part of the image you want to keep*

![cropped window](../../images/assignment2/image27.jpg)
*press return to crop*

## Export

Choose Export... from the File menu and you are asked to choose a name for the file. There wasn't a way to choose the file format so I just typed filename.jpg and hit return. This in turn opened an Export Image as JPEG dialog wherein I could set the parameters for my image export:

![export dialog](../../images/assignment2/image28.jpg)
*the export dialog*

...and that was it! Overall, Gimp is a really straightfoward image editing program with controls akin to photoshop. I'm definitely interested in using this more moving forward.

# 3D Modeling Tools

I have been looking forward to getting familiar with [Autodesk Fusion 360](https://autodesk.com) since I saw it being used in a class last year. I'm really interested in getting familiar with this software because of how many different things it can do. I'm particularly interested building schematics and designing electronic boards. However, I started at the very beginning here, attempting to take the faceplate I designed in InkScape and bring it to a more concrete place in Autodesk.

## Create a sketch

Almost everything in this program starts with a sketch. You click the sketch button and then choose an axis on which to sketch. Alternatively, you can select a pre-existing object and create a sketch on its surface.

![create sketch](../../images/assignment2/image11.jpg)
*create sketch button*

![axis planes](../../images/assignment2/image12.jpg)
*choose an axis plane for the sketch*

## Rectangle Tool

Once in sketch mode, there are several ways to start sketching. As with the vector-based design, I started with a rectangle by selecting the appropriate button at the top of the screen:

![create a rectangle](../../images/assignment2/image13.jpg)
*create a rectangle*

You select an origin point by clicking and then drag in any direction to start sketching your rectangle. You can also type the specific length and width at any time, switching between them with the ```tab``` key.

![sizing the rectangle](../../images/assignment2/image14.jpg)
*add dimensions*

## Extrude

After you have formed your sketch, click "Finish Sketch" and then press ```e``` to initiate the **extrude** command. This adds depth to your sketch. If it's not already selected, click the rectangle and you will be given options with which to extrude the shape. 

![extrude sketch](../../images/assignment2/image15.jpg)
*the extrude dialog*

## Create a cutout

Next I wanted to create a cutout in the main rectangle. To do this, I started a new sketch, using the existing rectangle as the surface and then drew out a rectangle as before. 

## Dimensions & Constraints

To create a dimension between one line and another, just press ```d```, then choose two lines that you want to define a dimension between and click a third time to place that dimension on the sketch:

![dimension](../../images/assignment2/image16.jpg)
*adding dimensions*

Dimensions are useful but constraints are more powerful for parametric design. When possible, you should constrain the sketch rather than specifying absolute measurements. This will allow you to more easily make changes later because if one dimension changes, everything else will change automatically if constrained properly. 

![constraints menu](../../images/assignment2/image21.jpg)
*many ways to constrain your sketches*

## Appearance

For 3D modeling, you can use the appearance function to add surface texture to your designs. You access this by pressing ```a```. The dialog pops up and there is a large bank of materials to choose from. Make sure the material has been downloaded by clicking the down-pointing arrow and then drag the material onto the body you want to add texture to.

![appearance](../../images/assignment2/image22.jpg)
*add texture to your design*

## Timeline Tool

The timeline tool is a very powerful way to edit your design without having to restart. Essentially, you are shown icons for every major event you have completed in your design. You can double click any of these to go back to that moment in time, make changes and then go "back to the future" with the changes taken into account!

![Timeline tool](../../images/assignment2/image23.jpg)

*Back to the Future with the timeline tool*

## Reference Image

You can insert a reference image as seen below. This is useful for modeling complex shapes, or in my case just making it more convenient to remember where the holes should go.

![insert reference image](../../images/assignment2/image30.jpg)

*insert reference image*

With the reference image in place, I was more easily able to add holes for the potentiometers and inputs.
![reference image side by side](../../images/assignment2/image31.jpg)
*reference image next to aluminum face plate*

## Rectangular Pattern

I used the rectangular pattern to create 4 evenly spaced screw holes in the corners of the aluminum to attach the synth to a rack. The rectangular pattern allows you to choose how many copies there are and how they are spaced out. 
![rectangular pattern](../../images/assignment2/image32.jpg)

*choose rectangular pattern*

## Move/Copy

Because I had so many similar sized holes in my design, I used the copy/move command to duplicate the holes. I selected the element I wanted to copy, right-clicked and then chose copy/move. This opens a dialog where you can find a "create copy" box at the bottom. 

![move/copy](../../images/assignment2/image33.jpg)

*move/copy dialog*

After creating, copying and distributing all the holes, I selected them all and extruded at the same time, cutting through the main element. Finally, I had a pretty decent mock up of my aluminum faceplate.
![finished face plate](../../images/assignment2/image34.jpg)
*faceplate in a more complete state*