+++
title = '05 3D Printing and Scanning'
author = 'Shane Wirkes'
date = 2024-02-27T08:42:51+02:00
draft = false
+++

Scanning and printing...in 3D!
<!--more-->

## Group Training: Prusa & Ultimaker & Creality

As a group, we learned about using the Prusa and Ultimaker printers as well as their respective slicing software. Then we went through and introduction to 3D scanning with the Creality Ferret Pro. Documentation can be found [here](https://zinaaaa.gitlab.io/digital-fabrication/week6_docu.html).

## 3D Scanner

I used the Creality Ferret Pro with the wireless bridge and attached it to my phone. 

![creality scanner](../../images/assignment5/image18.jpg)

After downloading the app (Creality Scan for iOS), I powered on the scanner and selected the Wi-Fi network on my phone that the scanner created. 

![wi-fi network](../../images/assignment5/image16.jpg)

The first thing I tried to scan was my face. I was pretty impressed and very creeped out with the quality. I aimed the camera at myself and slowly moved my head around. It's  not the cleanest scan, but considering it rendered from a mobile phone, I think it's pretty cool.

{{< video "../../videos/face_scan.mp4" >}}

I wanted to try to get a cleaner scan and I was having issues with the scanner making me reset a lot with the mobile app, so I downloaded the macOS app on my computer and tried again, using the Wi-Fi bridge.

The set up was nearly identical to the mobile app, however there was an option for Turntable, meaning you could set the object to be scanned on a rotating table and the camera would adjust accordingly.

I spotted the rotating Jesus at the fablab and decided to give it a shot. 

I let the piece rotate two and a half times and saw the scan was mostly green. I then finished the scan and let the program optimize.

![optimizing](../../images/assignment5/image5.jpg)

The finished result was much better quality. I believe this was mostly because I had the scanner stationary on a tripod and the object to scan was revolving independently. There was no shaking of the camera or other human error involved.

{{< video "../../videos/jeezus_scan.mp4" >}}


I exported the file as a png and it created a compelling image:

![jeezus pieces](../../images/assignment5/jeezuspiece.jpg)

Luckily, I got the screenshots and video before the program crashed and the image disappeared. However, the program did save the scan as an obj file.

## 3D Printing

I designed a test object in Fusion 360 to print. I started with a tutorial designing a painter's pyramid. Essentially, it's a pyramid with three sides with holes in the middle of each side and an open bottom. 

They are used to stand objects up that need to be painted or dried on the bottom. You rest them on these pyramids to minimize the surface area touching the pyramids.

I started with an equilateral triangle and then created a projection of the triangle several milimeters above. I then found the center point of that projection and used the ```loft``` command to create the pyramid:

![lofting the pyramid](../../images/assignment5/image8.jpg)

Then I hollowed out the pyramid using the ```shell``` command.

![shell command ](../../images/assignment5/image9.jpg)

The next step was to add the circles which I did with the ```create circle at centerpoint``` command and adding one to the centerpoint of one side of the pyramid.

![circles on the side](../../images/assignment5/image10.jpg)

With the circle in place, I extruded it using the cut setting.

![extrude circle](../../images/assignment5/image11.jpg)

With one of these in place I was able to use the ```circular pattern``` command to add the same circle to each face of the pyramid.

![circle pattern](../../images/assignment5/image12.jpg)

Then, to make this design my own and add a feature that would make it impossible to create with subtractive manufacturing, I added a sphere inside the pyramid.

![add sphere](../../images/assignment5/image13.jpg)

With the sphere in place, I closed off the bottom pyramid using the projected points from the Top Point sketch I made at the beginning.

![close bottom](../../images/assignment5/image14.jpg)

With the sphere in place I adjusted the position and scaling so that it wasn't touching any part of the pyramid. To make sure I used the ```Inspect``` -> ```Interference``` command, which analyzes the position of 2 or more objects and lets you know if they are touching at all.

Finally, I used the ```Shell``` command one more time on the sphere to hollow it out and save material on the print.

From the Design menu, I went to ```Utilities``` -> ```MAKE``` -> ```3D Print``` and I was able to export my design as an STL file.

I then took the STL file to the Cura slicer program and set it to Draft. Because I had the ball floating in the original design, I added supports and verified that they were there holding up the ball. 

![print with brim](../../images/assignment5/image25.jpg)

I printed the object with a brim because the bottom is solid and I thought it would make it easier to lift from the plate (it didn't).

The object turned out fine. The supports were easy to remove.

![with supports](../../images/assignment5/image26.jpg)

*with supports*

![supports removed](../../images/assignment5/image27.jpg)

*supports removed*

I wasn't able to remove the supports from the top of the sphere inside the pyramid. I would be interested to try this design with the water soluble material for support and see how it works.
