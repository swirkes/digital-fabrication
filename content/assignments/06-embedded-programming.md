+++
title = '06 Embedded Programming'
author = 'Shane Wirkes'
date = 2024-03-03T08:42:51+02:00
draft = false
+++

Programming the XIAO RP2040
<!--more-->

I decided to use [PlatformIO](https://platformio.org/) with VS Code to program the board. You can install the PlatformIO extension in VS Code just as you would any other extension.

![extensions marketplace](../../images/assignment6/image14.jpg)

An alien icon appears on the left toolbar after installation which opens the main page for PlatformIO

![Welcome to PlatformIO](../../images/assignment6/image5.jpg)

At first, I tried to use the project wizard to set up the board automatically:

![project wizard](../../images/assignment6/image6.jpg)

This didn't quite work and after searching the forums, I found out why. 

When you make a new PlatformIO project, a platformio.ini file is created. This is where you store the information for the board, port, libraries, and anything else that might be needed to connect with the hardware.

Because this is a XIAO-RP2040, I had to use some custom settings which I found on the [forums](https://community.platformio.org/t/support-for-seeed-xiao-rp2040/28989).

```
[env]
platform = https://github.com/maxgerhardt/platform-raspberrypi.git
framework = arduino
board_build.core = earlephilhower
board_build.filesystem_size = 0.5m

[env:seeed_xiao_rp2040]
board = seeed_xiao_rp2040
```

The next thing I needed to do was convert the .ino file I used to test the board last week into a .cpp file. Because this was a simple sketch, the only thing I needed to do was import the Arduino library and save as... a .cpp file.

![#include Arduino](../../images/assignment6/image7.jpg)

At the bottom of the VS Code window, there are buttons for verifying and uploading sketches. The checkmark verifies the code and the right-pointing arrow uploads it to the board.

![upload button](../../images/assignment6/image10.jpg)

A button shaped like an electrical plug lets you choose the port. When I clicked this, I was able to see the port through which my board was connected.

![choose port](../../images/assignment6/image11.jpg)

I successfully uploaded the code to the board, but I couldn't verify it since I was using the same code that was already on the board. So I added some code that would blink the built in LED with a one second delay in the loop.

![blink built in LED](../../images/assignment6/image12.jpg)

With this code uploaded and the LED blinking, I verified that I had successfully used PlatformIO to upload code to my board.
