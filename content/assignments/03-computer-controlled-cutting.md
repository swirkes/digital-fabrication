+++
title = '03 Computer Controlled Cutting'
author = 'Shane Wirkes'
date = 2024-02-13T18:42:51+02:00
draft = false
+++

Stepping into laser and vinyl cutting...
<!--more-->

## Group Training: Laser Cutter

We worked as a group to learn about the safe operation of the laser cutter and determine the kerf of the laser. A detailed explanation of our 
work was documented by Graham Featherstone and can be found [here](https://grahamfeatherstone.gitlab.io/digital-fabrication/Projects/LaserTraining/Laser.html).

## Laser Cut a press fit construction kit

I wanted to make something from scratch rather than copy an idea directly from YouTube, but this proved a little more difficult for me than I had thought. After thinking (probably too much) about what to make, I came up with an idea to create some new shapes for these toy blocks my daughter recently received:

![simple kid's blocks](../../images/assignment3/IMG_5672.jpg)

![simple kid's blocks 2](../../images/assignment3/IMG_5673.jpg)

I started by taking measurements of the block and then used inkscape to recreate the block directly.

## Designing for Laser Cutter

I started with the rectangle tool, however, when I would create a rectangle, it would not show up. If I selected over the area, an outline appeared, but the shape wasn't visible. The problem took me quite a while to solve, so I want to document it here for future reference.

**TROUBLESHOOTING!**
First I checked the Fill and Stroke window, assuming that this was the problem. I determined that even with stroke and fill set to black and the opacity fully off, the shape still wasn't visible.

Then I checked the layer tool to make sure that it wasn't somehow hidden behind a layer I wasn't aware of. This was not the case. However, I saw a square with a slash next to the shape I made and clicking it revealed another opacity setting which was at 0%. 

![layer opacity at 0%](../../images/assignment3/image15.jpg)

After figuring this out, I googled the problem and found that you can modify the presets for shapes in the preferences menu:

![modifying the preferences](../../images/assignment3/image9.jpg)

In fact, if you select the shape you want to base your preset on before going to preferences, you can click the "Take from selection" button to copy that setting. 

Okay, back to the laser cutting...

## Shape Building

I used the height and width boxes in the toolbar to ensure my shapes were exactly the size I needed. 
[height and width toolbar](../../images/assignment3/image1.jpg)

I stripped everything down to rectangles and stacked them to make a composite of the final shape.

![singular rectangles](../../images/assignment3/image2.jpg)

![stacked rectangles](../../images/assignment3/image3.jpg)

After this, I used the Shape Builder tool to combine these rectangles into a single shape:

![shape builder tool](../../images/assignment3/image4.jpg)

The resulting shape was exactly what I was looking for:

![final shape](../../images/assignment3/image5.jpg)

Starting from this basic shape, I created several more shapes that would fit with these blocks. I used a similar process to above, starting with single shapes and building them up into more complex ones with the shape builder tool.

![shapes layed out](../../images/assignment3/shapesinkscape.jpg)

## Prepare for laser cutter and adjust for kerf

With the shapes layed out, I adjusted the stroke of all the shapes to 0.01 mm so that the laser cutter would recognize it as a cut. I needed to adjust for kerf as well. 

The kerf of our laser was found to be 0.28 mm, so I needed to offset my cuts by 0.14 mm. The way to do this in inkscape is with Path Effects. This can be found under the Path menu or by pressing ```CMD + &```. 

![path effects](../../images/assignment3/patheffects.jpg)

Select the shape you want to edit and then in the Path Effects dialog, you can search for Offset. This allows you to adjust for kerf precisely.

## Laser Cutting

With the file properly formatted, I took to the laser cutter. 

The first thing to do is make sure the fume extractor is on. The switch is at the door. There must always be someone present at the laser cutter while it is on, so a small box next to the cutter scans a card or takes a button press every 5 minutes to ensure that a person is present. You scan the card to start the laser cutter. 

![fume extractor](../../images/assignment3/image13a.jpg)

Next, log into the computer like you would any Aalto computer and open up your editor. I used Inkscape. Load your file and check that it looks okay.

Click Print... then choose Epilog Engraver as the printer and click print again to open the Epilog print manager. 

On the left side of the screen there is an image feed from a camera on the laser printer, showing you where your material is set. You can adjust the active window to fit your design as well as move the design around in the window. 

![epilog print dialog](../../images/assignment3/image15a.jpg)

On the right, there are three main settings to look at: Speed, Power, and Frequency. In our training session, we made a test card with a matrix of progressively larger speed and power values on the X and Y axis. With this card in hand, I set the speed to 40% and the power and frequency to 50%. 

With these set, you can click send to JM which opens a new window with a Jobs tab. Clicking the Jobs tab will reveal the job that you are working on. Select that job and click the Quick Print button. The job has now been sent to the printer. 

The printer has a screen which now shows your job. Ensure that job is selected and press the checkmark button. The print will start. 

![printer interface](../../images/assignment3/image14a.jpg)

In the image below, I had initially set the speed too fast (50%) and the laser didn't cut all the way through my material. I tried to run the job again, and lined up the design with the lines on the cardboard as exactly as I could, but right away I could tell it wasn't turning out nicely. You can press the button that has two arrows in a circle if you want to stop the job. I ended up flipping the cardboard around and starting fresh.

![printer](../../images/assignment3/IMG_5668.jpg)

Here are the finished shapes:

![finished shapes](../../images/assignment3/IMG_5669.jpg)

And finally to make sure I printed them exactly to specification, I combined them with the original blocks:

![printed and original blocks 1](../../images/assignment3/IMG_5674.jpg)

![printed and original blocks 2](../../images/assignment3/IMG_5675.jpg)

## Using the Vinyl Cutter

I took a photo of an design that I wanted to make a sticker out of:

![image scan](<../../images/assignment3/Endless knots scan.jpg>)

*image taken from Encyclopedia of Tibetan Symbols and Motifs by Robert Beer*

Then importing it into Inkscape, I traced the bitmap to get a vector image.

![trace bitmap](<../../images/assignment3/trace bmp.jpg>)

After this, I exported the file as an SVG and I was ready to go to the vinyl cutting station.

First things first, turn on the printer with the power button on the main interface.

![vinyl printer interface](../../images/assignment3/image1a.jpg)

Then, lift the handle on the back of the printer so that the rollers lift up and you can place your material.

![back of vinyl printer](../../images/assignment3/image2a.jpg)

There are three ways to load the material into the printer. Back, Front, and Edge. I chose Front, because I was using a relatively small piece of vinyl for my sticker. Adjust the rollers from behind so that one is on each side of the material and both are within one of the white boundary markers just above the rollers on the printer. 

![vinyl printer roller](../../images/assignment3/image3a.jpg)

Here, the vinyl is properly loaded into the printer:

![vinyl loaded into front of printer](../../images/assignment3/image4a.jpg)

I pulled my SVG into Adobe Illustrator and because of the new version, I needed to save it as an EPS file before moving onto the next step.

![save as EPS](../../images/assignment3/image6a.jpg)

Then, I opened CutStudio and imported the EPS file

![import into CutStudio](../../images/assignment3/image7a.jpg)

My design appeared in CutStudio as expected, so I clicked print and chose the Roland printer:

![design in CutStudio](../../images/assignment3/image8a.jpg)

The design required a lot of weeding. Luckily there was a really nice tool (dental pick?!) that got me where I needed to be.

![weeding sticker](../../images/assignment3/image9a.jpg)

The sticker was all weeded and from there it was easy to peel it back to adhere it somewhere.

![finished weeding](../../images/assignment3/image10a.jpg)

I put it on my iPad case. It turned out pretty well!

![sticker adhered to ipad case](../../images/assignment3/image12a.jpg)