+++
title = '04 Electronics Production'
author = 'Shane Wirkes'
date = 2024-02-21T18:42:51+02:00
draft = false
+++

Milling and soldering a custom PCB...
<!--more-->

## Group Training: Roland MDX-40

Burak gave our group a thorough introduction to PCB milling and SMD soldering. A detailed explanation of our work was documented by myself and can be found [here](https://swirkes.gitlab.io/digital-fabrication/group-work/group-training-mdx-40/).

![milled and populated board](../../images/assignment4/image8.jpg)

*my milled and populated board* 

## Flashing the board

After milling the board and soldering the components as documented above, it was time to flash the board with a test program. 

### Arduino IDE

It was recommended to use the older version 1.8.X Arduino IDE, but since I already had the updated version 2.3.1 on my MacBook, I decided to give it a shot. 

Indeed, it worked just fine and I got the LED blinking on the first try. Here's how it went down...

The first thing I did was plug in the board to my computer, making sure I had the RESET button the XIAO depressed as I did. Additionally, I ensured that I was using a USB C cable that was intended for data transfer and not just power.

With the correct cable connected, the board showed up as a removable disk on my Desktop:

![Disk mounted on Desktop](../../images/assignment4/image1.jpg)

Next, I went to settings by pressing ```CMD- , ``` and at the bottom of the window, inserted the URL I got from the instructions on the [seeed studio](https://wiki.seeedstudio.com/XIAO-RP2040-with-Arduino/) website. 

![Additional boards manager](../../images/assignment4/image2.jpg)

I then navigated to the **boards manager** from the Tools menu:

![boards manager](../../images/assignment4/image3.jpg)

From here I was able to search for and install (or in my case update) the corresponding package:

![Install package](../../images/assignment4/image4.jpg)

With the package installed, I am now able to choose the RP2040 as a board from the Tools menu.

![Select Board](../../images/assignment4/image5.jpg)

I copy/pasted the ```hello_tarantino.ino``` code into the open file and saved it before clicking the checkmark at the top of the screen to verify the code.

![verify code](../../images/assignment4/image6.jpg)

After verifying the code, I clicked the right-facing arrow next to the checkmark to upload the code to the board. The terminal output at the bottom of the screen told me that the code had been uploaded successfully:

![terminal output](../../images/assignment4/image7.jpg)

Finally, back in the physical world, I tested the device to ensure that the expected behavior had been programmed. Success!

{{< video "../../videos/blinkboard_optimized.mp4" >}}
