+++
aliases = ["assignments", "articles", "blog", "showcase", "docs", "posts"]
title = "Assignments"
author = "Shane Wirkes"
description = "Assignment documentation for Digital Fabrication 2024"
tags = ["index"]
draft = false
+++
