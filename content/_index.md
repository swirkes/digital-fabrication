+++
title = 'About'
date = 2024-01-31T10:38:35+02:00
draft = false
+++

![Shane Headshot](images/shane_bio_pic.jpeg)

# About Me
Hi, I'm Shane. I have a background in music and embedded programming. I'm currently working towards a master's degree in Sound in New Media centered around the creative use of Software Defined Radio. I'm excited about designing novel musical interfaces and searching for new sounds with them.

# My Work
Here are some links to my other work:

- [My personal website](https://shanewirkes.com)
- [My bachelor's thesis](https://shanewirkes.com/environmentally-reactive-sound-design/)
- [Living Still Life Sound Installation](https://shanewirkes.com/living-still-life/)
- [Radio-based Sound Installation](https://shanewirkes.com/static-chatter-sound-installation/)
- [My music on Spotify](https://open.spotify.com/artist/6BOEjeBVIHfUZkkA1ccri5)
