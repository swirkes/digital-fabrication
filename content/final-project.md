+++
title = 'Final Project'
author = 'Shane Wirkes'
date = 2024-01-31T10:54:22+02:00
draft = false
+++

I am planning to build a synthesizer with a radio receiver as the core of the synthesis engine. It will be built in the spirit of [Eurorack](https://en.wikipedia.org/wiki/Eurorack) module, which means that by itself, it will have limited functionality, but its inputs and outputs will be designed to feed in and out of other standard Eurorack modules.

There will be 2 knobs, one for tuning the frequency and one for adjusting the filter width. Additionally, there will be 2, 3.5 mm audio jack inputs that accept ±5V control voltage (CV) to adjust the tuning and filter width (e.g. with an LFO).

There will be 3, 3.5 mm outputs. One will be a headphone jack to monitor the radio signals. The other two will be audio out and CV out. The CV out will convert the radio signal to a control voltage between ±5V and send it out to control other modules.

A digital screen at the top of the module will show the frequency and hopefully a waterfall graph or FFT display.

I would like to use an IC for the radio circuit for more flexibility with the tuning range and to make it smaller (and so that it runs cooler).

If time allows, I would like to use capacitive touch strips to be able to tune and adjust the bandwidth with touch.

![Sketch of the radio synthesizer module](../images/sdrModularSketch.jpg)

## Components

After searching for an approachable FM radio chip, I found a [breakout board](https://www.sparkfun.com/products/11083) for Silicon Laboratories [Si4703](https://media.digikey.com/pdf/Data%20Sheets/Silicon%20Laboratories%20PDFs/Si4703-B16.pdf) FM tuner chip. The breakout board has connections for all the major pins including stereo output, GND and VCC, and Antenna. There is another version that also includes an amplifier and micro TS audio output. 

![breakout board](<../images/final project/si4703_breakout.jpg>)

I started going through the data sheet and one of the first things I found was that this breakout board is for 3.3 Volt power sources. This includes the logic lines coming from a microcontroller. 

Luckily, Karl Mihhels worked on a solution for this during his Fab Academy in 2022:

![logic level shifter](https://karlpalo.gitlab.io/fablab2022/weekly/levelshifter.jpg)

Using a MOSFET in this configuration allows you to modify the voltage from 5 to 3.3 Volts or vice versa. 

## Schematics

I found an open source schematic from [Music From Outer Space](https://musicfromouterspace.com/index.php?MAINTAB=SYNTHDIY&PROJARG=EXPERIMENTERBOARD/page1.html&VPW=1430&VPH=632). This website is a treasure trove of free schematics for analog synth hobbyists. The design is made for through hole components, so I am attempting to convert it to SMD and create a smaller footprint. Additionally, instead of making it a separate module, I'll try to combine them.

![vco schematic](<../images/final project/vco-schematic.jpg>)

I realized after working on the schematic that I need to use nets and labels to reduce the amount of messy wires all over the design.