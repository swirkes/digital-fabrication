+++
title = 'MDX-40 Group Training'
author = 'Shane Wirkes'
date = 2024-02-16T10:42:51+02:00
draft = false
+++

Learning how to use the MDX-40 for PCB milling
<!--more-->

## MDX-40

Here is the machine.

![mdx-40](../../images/MDX-40/image1.jpg)

The machine is generally used for 3D sculpting and can make curves like this:

![3d sculpting](../../images/MDX-40/image2.jpg)

In our case, we're just using it to mill flat PCBs.

![control panel](../../images/MDX-40/image3.jpg)

Turn on the machine with the Power button and press View to bring spindle and bed to a more easily acessible place.

Use two wrenches to loosen the collet in order to replace it with the PCB milling bit's collet.

![wrenches](../../images/MDX-40/image4.jpg)

Set some soft material under the bit while loosening in case it falls.

![soft material in case it falls](../../images/MDX-40/image5.jpg)

Here are the collets. We're using the ones for PCB milling.

![collets](../../images/MDX-40/image7.jpg)

Attach the new collet to the spindle and tighten with the two wrenches. Tighten snugly but don't overtighten.

![adding the collet to spindle](../../images/MDX-40/image8.jpg)

Now we can add the milling bit. Here is the box of them. We're using the 0.2 - 0.5 mm milling bit and the 0.8 mm cutting bit

![pcb milling bits](../../images/MDX-40/image9.jpg)

Attach the bit to the spindle and use the hex key (the one attached to the machine with a magnet) to tighten it in. Again, don't overtighten, just make it snug.

Get your copper board and measure the thickness to double check for the settings on the machine.

![copper boards](../../images/MDX-40/image10.jpg)

![measure copper board width](../../images/MDX-40/image11.jpg)

*use a caliper to measure the thickness*

Add some tape to the back of the board to affix it to the bed. This keeps it stable and also removes the natural bend from the material so that you can mill with precision.

![Add tape to back of board](../../images/MDX-40/image12.jpg)

*add tape to the back of the board*

![tape affixed to board](../../images/MDX-40/image13.jpg)

*tape affixed*

The board doesn't need to be precisely laid down, just try to get it pretty straight.

![set with tape](../../images/MDX-40/image14.jpg)

*set the board into the bed*

Now we move to the computer program.

Open CopperCAM

Select Open... New Circuit and select the Gerber file we are using for the board.

Make sure the Z-Thickness is the material thickness. We measure the board at just over 1.5 mm, so we'll set it to 1.6 mm to make sure it can go all the way through.

![Board Dimensions in CopperCAM](../../images/MDX-40/image30.jpg)

The Card Contour path is the outline of the board, the part that will be cut out from the sheet.

Go to File... Open... Drills

If not aligned, select layer 1, click Set Reference Pad, select Drill layer, right click hole to align, select adjust to reference pad.

If given the option between npth and pth, **select pth**

Now go to parameters -> Tool Library

Tool 1 = 0.02 - 0.05 engraver
Tool 2 = .08 cutter

Go to Selected Tools and make sure the correct tools (from above) are set in Engraving and Cutting.

The Drilling Depth should be the same as the Z-Depth from before (e.g. 1.6 mm).

Go to Calculate Contours

Set contours -> 4 is a good number to reduce the amount of copper, making it easier to solder later on.

Extra contours around pad can be set to 1

![Set Contours in CopperCAM](../../images/MDX-40/image31.jpg)

If you have text on the board, select the letters, right click, and choose Engrage as center lines.

Click Mill

Check that sections 1, 2, and 3 are filled out appropriately.

Make sure "mirror X" is **unchecked**

X,Y position should be set to Southwest

Click OK and then Save as... be sure to save as a .nc file

This will generate 2 files, one for Tool 1, the engraver, and one for tool 2, the cutter.

## Open V-Panel

Set up -> make sure nc code is selected and that everything that can be set to G54, is set to G54.

### Set origin

![VPanel screen](../../images/MDX-40/image29.jpg)

Move the bed around until the bit is roughly above the bottom right corner and click Set XY Origin.

For the Z origin, lower the bit carefully over the board, then loosen the collet and let it fall onto the material. Tighten it up again and click Set Z-Origin.

### Click Cut...

Add... Choose the first .nc file

### Click output

Don't start from 0,0,0 origin, rather move the Z up a bit, and up and over on the X,Y plane a bit.

Click Output to begin the milling.

After it's done, check out what you've got by pressing View on the control panel.

![first pass milled circuits](../../images/MDX-40/image15.jpg)

![first pass finished and dusted](../../images/MDX-40/image17.jpg)

Now change the bit, same as before but add the 0.08 cutting bit.

Re-align and recalibrate the Z-position by letting the bit drop onto the material. 

Now when you go to Cut... Delete all files, Add... choose the second file, click Output to run it again.

When the machine is finished, vaccuum out the dust and then use some metal spatulas to scrape it away from the tape.

![vaccuum out dust](../../images/MDX-40/image16.jpg)

![scrape board off](../../images/MDX-40/image18.jpg)


## Soldering

Before starting with soldering, collect all your materials. 

Locate the BOM (bill of materials) 

![BOM2](../../images/MDX-40/image20.jpg)

Collect your components (for this project, our components are already collected in a box!)

![SMD resistor](../../images/MDX-40/image21.jpg)

*SMD resistors are quite small*

At your soldering station, turn on the ventilator. Next turn on your soldering iron and set it to somewhere around 320 degrees. 

![soldering iron](../../images/MDX-40/image22.jpg)

![add flux](../../images/MDX-40/image23.jpg)

The general process is:

- add flux to the pads you will solder with a flux pen
- add a small amount of solder to one pad
- place the component and hold down with tweezer
- heat the pad with solder to pin the component down
- solder the other point(s)
- resolder the initial point

Finally, to add rivets to the holes you have drilled:

- put in the rivet through the back of the board
- lay it down right side up using a piece of paper to keep it in place
- use a small punch tool to fasten it to the board
- use a slightly larger punch tool to flatten out the hole
- apply a small amount of solder on the rivet where it goes to the copper lead

![pcb rivets](../../images/MDX-40/image26.jpg)

A note about LEDS: They are polarized, so make sure that the side with the small green line is going to Ground. This is the negative side of the LED.


















